﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OrgerLab.Modules.Core;
using OrgerLab.Modules.Display;
using OrgerLab.Modules.NI.Camera;
using OrgerLab.Modules.Recording;
using OrgerLab.Modules.Tracking;
using OrgerLab.Modules.Transform;
using System.Numerics;

namespace HeadRestrainedTracking
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Acquisition
        Camera camera;

        //display
        GenericFastForwardDisplay imageDisplay;

        //Tracking
        TailTracker tailTracker;
        EyeTracker eyeTracker;

        //Transform
        BackgroundSubtractor backgroundSubtractor;

        //Recording
        DataRecorder dataRecorder;
        VideoRecorder videoRecorder;


        //Application Modules
        List<Module> AppModules;


        private bool experimentIsRuning = false;
        public bool ExperimentIsRunning
        {
            get { return experimentIsRuning; }
            set { experimentIsRuning = value; Changed("ExperimentIsRunning"); }

        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            // Load parameters
            AcquisitionParameters.LoadSettings();
            mainCanvas.Height = CanvasBorder.Height = 500;
            mainCanvas.Width = CanvasBorder.Width = 400;

            // Display
            Type[] displayableData = new Type[2] { typeof(MPTTrackingResult)  , typeof(FloodFillResult) };
            imageDisplay = new GenericFastForwardDisplay(mainCanvas, displayableData);
            imageDisplay.ModuleName = "Display";

            // Recording
            List<Type> typesToSave = new List<Type>
            {
                typeof(MPTTrackingResult),
                typeof(FloodFillResult),
                typeof(CameraSessionData),
            };
            dataRecorder = new DataRecorder(typesToSave);
            dataRecorder_UI.Bind(dataRecorder);

            videoRecorder = new VideoRecorder();
            videoRecorder_UI.Bind(videoRecorder);

            // Tracking
            tailTracker = Module.LoadSettings<TailTracker>() ?? new TailTracker();
            tailTracker_UI.Bind(tailTracker);

            eyeTracker = Module.LoadSettings<EyeTracker>() ?? new EyeTracker();
            eyeTracker_UI.Bind(eyeTracker);

            // Background Subtraction
            backgroundSubtractor = Module.LoadSettings<BackgroundSubtractor>() ?? new BackgroundSubtractor();
            backgroundSubtraction_UI.Bind(backgroundSubtractor);
            backgroundSubtractor.ModuleName = "BG Subtraction";



            // Camera
            camera = new Camera();
            camera.ModuleName = " Mikrotron MC1362";

            // Module Manager
            AppModules = new List<Module>()
            {
                    imageDisplay,
                    dataRecorder,
                    videoRecorder,
                    eyeTracker,
                    tailTracker,
                    camera,
                    backgroundSubtractor,
                };

            AppModules = AppModules.FindAll(delegate (Module m) { return m != null; });
            moduleManager.Modules = AppModules;
            AppModules.ForEach(m => m.ChangeModeTo(Module.Mode.CALIBRATION));
        }

        private void canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (eyeTracker_UI.calibrateL)
            {
                Point p = e.GetPosition(sender as Canvas);

                p.X = Convert.ToInt32(p.X);
                p.Y = Convert.ToInt32(p.Y);
                eyeTracker.Alg.LeftEye = p;

                eyeTracker_UI.calibrateL = false;
            }
            else if (eyeTracker_UI.calibrateR)
            {
                Point p = e.GetPosition(sender as Canvas);
                p.X = Convert.ToInt32(p.X);
                p.Y = Convert.ToInt32(p.Y);
                eyeTracker.Alg.RightEye = p;

                eyeTracker_UI.calibrateR = false;
            }
            else if (tailTracker_UI.calibrateStart)
            {
                Point p = e.GetPosition(sender as Canvas);

                p.X = Convert.ToInt32(p.X);
                p.Y = Convert.ToInt32(p.Y);

                tailTracker.Alg.FirstBodyPoint = new Vector2((float)p.X, (float)p.Y);

                tailTracker_UI.calibrateStart = false;
            }
            else if (tailTracker_UI.calibrateEnd)
            {
                Point p = e.GetPosition(sender as Canvas);

                p.X = Convert.ToInt32(p.X);
                p.Y = Convert.ToInt32(p.Y);

                tailTracker.Alg.LastBodyPoint = new Vector2((float)p.X, (float)p.Y);

                tailTracker_UI.calibrateEnd = false;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            AppModules.ForEach(m =>
            {
                if (m != null)
                {
                    m.IsActive = false;
                    m.ChangeModeTo(Module.Mode.IDLE);

                    m.SaveSettings();
                }
            });
            AcquisitionParameters.SaveSettings();
        }

        private void StartExperiment(object sender, RoutedEventArgs e)
        {
            if (!ExperimentIsRunning) //START experiment
            {
                AppModules.ForEach(m =>
                {
                    if (m.ModuleMode == Module.Mode.CALIBRATION || m.ModuleMode == Module.Mode.RUNNING)
                    {
                        m.ChangeModeTo(Module.Mode.RUNNING);
                        //if module failed to transit to RUNNING stop the module to save resources
                        if (m.ModuleMode == Module.Mode.CALIBRATION)
                            m.ChangeModeTo(Module.Mode.IDLE);
                    }
                });
            }
            else //STOP experiment
            {
                AppModules.ForEach(m =>
                {
                    if (m.ModuleMode == Module.Mode.RUNNING)
                        m.ChangeModeTo(Module.Mode.CALIBRATION);
                });
            }
            ExperimentIsRunning = !ExperimentIsRunning;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void Changed(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}